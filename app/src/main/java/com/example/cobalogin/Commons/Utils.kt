package com.example.cobalogin.Commons

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor


class Utils {
    companion object {

        val DEV_ENDPOINT = "http://tagdev.mncplay.id/"
        val LOGIN = "login"
        var created_by = ""
        val MENU = "menu"
        val TAKE = "take"
        var isSales = false
        val SALES = "sales"
        var FORM_IKR = ""
        var FORM_IKR1 = ""
        var FORM_IKR2 = ""
        var FORM_IKR3 = ""
        var FORM_IKR4 = ""
        var FORM_SALES = ""
        var ACCOUNT_ID =""
        val CAMERA = "camera"
        val CLEAR = "clear"
        val HIDECAM = "hidecam"
        val arrayImage:Array<String> = arrayOf("", "", "")
        var lat = ""
        var long = ""
        val RESET_SALES = "reset sales"
        val RESET_IKR = "reset ikr"
        val RESET_TAKE = "reset take"
        val LOC = "loc"

        fun buildClient(): OkHttpClient.Builder{
            val clientBuilder = OkHttpClient.Builder()
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            clientBuilder.addInterceptor(loggingInterceptor)
            return clientBuilder
        }
    }

}