package com.example.cobalogin.Services

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cobalogin.Commons.RxBaseFragment
import com.example.cobalogin.Commons.RxBus
import com.example.cobalogin.Commons.Utils
import com.example.cobalogin.R
import kotlinx.android.synthetic.main.fragment_menu.*
import java.util.zip.Inflater

class MenuFragment : RxBaseFragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onResume() {
        super.onResume()
        btnSales.setOnClickListener {
            Utils.isSales = true
            RxBus.get().send(Utils.SALES)
        }

        btnIKR.setOnClickListener {
            Utils.isSales = false
            RxBus.get().send(Utils.SALES)
        }

        btnTake.setOnClickListener {
            RxBus.get().send(Utils.TAKE)
        }

    }


}