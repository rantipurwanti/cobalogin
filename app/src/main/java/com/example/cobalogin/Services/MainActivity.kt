package com.example.cobalogin.Services

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.LocationManager
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.cobalogin.Commons.RxBaseActivity
import com.example.cobalogin.Commons.RxBus
import com.example.cobalogin.Commons.Utils
import com.example.cobalogin.Form.FormFragment
import com.example.cobalogin.Form.TakePicture
import com.example.cobalogin.Model.*
import com.example.cobalogin.R
import com.example.cobalogin.Login.LoginFragment
import com.github.florent37.camerafragment.CameraFragment
import com.github.florent37.camerafragment.CameraFragmentApi
import com.github.florent37.camerafragment.configuration.Configuration
import com.github.florent37.camerafragment.listeners.CameraFragmentResultListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_form.*
import okhttp3.OkHttpClient
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import rebus.permissionutils.PermissionEnum
import rebus.permissionutils.PermissionManager
import rebus.permissionutils.PermissionUtils
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.DateFormat
import java.util.*

class MainActivity : RxBaseActivity() {

    @SuppressLint("MissingPermission")
    val cameraFragment = CameraFragment.newInstance(Configuration.Builder().build())

    val dir = "/Pictures/MKM_Pictures/"
    var lat:String = ""
    var long:String = ""
    var currentNum:Int = 0
    var currentNum2:Int = 0

    //inisialisasi variabel untuk menyimpan data foto yang sudah diambil
    val arrayImageData:Array<ImageData> = arrayOf(
        ImageData("", "", "", "", ""),
        ImageData("", "", "", "", ""),
        ImageData("", "", "", "", ""))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //getLocate()
        if (savedInstanceState == null){
            getPermission()
            manageSubscription()
            changeFragment(LoginFragment(), false, "login")
            checkDir()
        }
    }

    override fun onResume() {
        super.onResume()

        btnRecord.setOnClickListener {
            if (Utils.isSales)
                handlerCameraSales()
            else
                handlerCameraTake()
        }
    }

    private fun handlerCameraSales() {
        val camFragment = getCameraFragment()
        camFragment.takePhotoOrCaptureVideo(object: CameraFragmentResultListener {
            override fun onVideoRecorded(filePath: String?) {
            }

            override fun onPhotoTaken(bytes: ByteArray?, filePath: String?) {
                Utils.arrayImage[currentNum] = filePath?: ""
                arrayImageData[currentNum] = ImageData(
                    "",
                    "${getImgName()}",
                    "${Utils.lat}",
                    "${Utils.long}",
                    "${getDateTime()}"
                )
                RxBus.get().send(Utils.HIDECAM)
                RxBus.get().send(Utils.SALES)
            }
        },
            Environment.getExternalStorageDirectory().toString() + dir,
            getImgName()
        )
        Log.d("formsales", Utils.FORM_SALES)
    }

    private fun handlerCameraTake(){
        val camFragment = getCameraFragment()
        camFragment.takePhotoOrCaptureVideo(object : CameraFragmentResultListener{
            override fun onVideoRecorded(filePath: String?) {
            }

            override fun onPhotoTaken(bytes: ByteArray?, filePath: String?) {
                Utils.arrayImage[currentNum] = filePath?: ""
                arrayImageData[currentNum] = ImageData(
                    "",
                    "${getImgName()}",
                    "${Utils.lat}",
                    "${Utils.long}",
                    "${getDateTime()}"
                )
                RxBus.get().send(Utils.TAKE)
                // RxBus.get().send(Utils.HIDECAM)
            }
        },
            Environment.getExternalStorageDirectory().toString() + dir,
            getImgName()
        )
    }

    //Fungsi untuk memvalidasi bahwa foto sudah terisi/diambil  atau belum
    private fun validateSendForm(event: Any) {
        if (Utils.arrayImage[0] != "" && Utils.arrayImage[1] != "" && Utils.arrayImage[2] != "")
            sendForm(event)
        else
            toast("semua foto harus terisi")
    }

    //variabel mutable variabel yang bisa diisi ulang
    fun sendForm(data: Any){
        val formData = mutableListOf<Any>()
        val loading = ProgressDialog.show(this, "Sedang mengirim data",
            "Mohon Tunggu...", false, false)
        val imageData = mutableListOf<ImageData>()
        formData.add(data)
        val size = Utils.arrayImage.size-1
        for (i in 0..size){
            val file = File(Utils.arrayImage[i])
            val bitmap = BitmapFactory.decodeFile(file.getAbsolutePath())
            arrayImageData[i].img_file = getStringImage(bitmap)
            imageData.add(arrayImageData[i])
            Log.d("dodol", "${imageData[i].datetime_taken}")
        }

        if (Utils.isSales){
            val form = FormSales(formData as MutableList<DataSales>, imageData)
            subscriptions.add(provideFormService().formSales(form)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                        resp ->
                    loading.dismiss()
                    toast("${resp.message}")
                    clearDataSales()
                }, {
                        error ->
                    loading.dismiss()
                    toast("pengiriman data gagal karena $error")
                }))
        } else {
            val form = FormIKR(formData as MutableList<DataIKR>,imageData)
            subscriptions.add(provideFormService().formIKR(form)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                        resp ->
                    loading.dismiss()
                    if(resp.code.equals("failed")){
                        toast("${resp.message}")
                    }
                    else{
                        toast("${resp.message}")
                        clearDataIKR()
                    }

                    //reloadImage()
                }, {
                        error ->
                    loading.dismiss()
                    toast("pengiriman data gagal karena $error")
                }))
        }

    }


    fun provideFormService(): MainService {

        val clientBuilder: OkHttpClient.Builder = Utils.buildClient()

        val retrofit = Retrofit.Builder()
            .baseUrl(Utils.DEV_ENDPOINT)
            .client(clientBuilder.build())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
        return retrofit.create(MainService::class.java)
    }

    //Fungsi untuk menyimpan bitmap kedalam file
    fun getStringImage(bmp: Bitmap):String {
        if (bmp != null) {
            val baos = ByteArrayOutputStream()
            bmp.compress(Bitmap.CompressFormat.JPEG, 25, baos)
            val imageBytes = baos.toByteArray()
            val encodedImage  = Base64.encodeToString(imageBytes, Base64.DEFAULT)
            return encodedImage
        }
        return ""
    }

    //Fungsi untuk berpindah fragment
    fun changeFragment(f: Fragment, cleanStack: Boolean, tag: String) {
        val ft = supportFragmentManager.beginTransaction()
        if(cleanStack)
            clearBackStack(supportFragmentManager)
        ft.replace(R.id.main_content, f, tag)
            .addToBackStack(null)
            .commitAllowingStateLoss()
    }

    fun clearBackStack(fm: FragmentManager) {
        if (fm.backStackEntryCount > 0){
            val first = fm.getBackStackEntryAt(0)
            fm.popBackStack(first.id, android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    fun manageSubscription(){
        subscriptions.add(RxBus.get().toObservable().subscribe{
                event -> manageBus(event)
        })
    }

    //FUngsi untuk memanage jalannya aplikasi
    fun manageBus(event:Any){
        when(event){
            Utils.LOC -> getLocation()
            Utils.RESET_SALES -> resetSales()
            Utils.RESET_IKR -> resetIKR()
            Utils.MENU -> changeFragment(MenuFragment(), false, "menu")
            Utils.SALES -> changeFragment(FormFragment(), false, "form")
            Utils.TAKE -> changeFragment (TakePicture(), false, "take")
            //Utils.CLEAR -> ClearTake()
            is ImageNum -> {
                layoutCamera.visibility = View.VISIBLE
                currentNum = event.num
                changeFragment(cameraFragment, false, "camera")

            }
            is ImageNumTake -> {
                layoutCamera.visibility = View.VISIBLE
                currentNum = event.num
                changeFragment(cameraFragment, false, "camera")

            }
            Utils.HIDECAM -> layoutCamera.visibility = View.GONE
            is DataIKR -> validateSendForm(event)
            is DataSales -> validateSendForm(event)

        }
    }

    //Fungsi untuk meminta izin
    fun getPermission(){
        PermissionManager.Builder()
            .permission(
                PermissionEnum.WRITE_EXTERNAL_STORAGE, //Memungkinkan aplikasi untuk menulis ke penyimpanan eksternal
                PermissionEnum.ACCESS_FINE_LOCATION, //Meminta izin aplikasi untuk mengakses lokasi yang tepat dari pengguna yang di dapat dari GPS dan lokasi jaringan
                PermissionEnum.READ_PHONE_STATE, //Memungkinkan hanya membaca akses ke negara telepon
                PermissionEnum.CAMERA) //Untuk mengakses perangkat camera
            .askAgain(false)
            .ask(this)
    }
/*    fun getLocate(){
        val granted = PermissionUtils.isGranted(this, PermissionEnum.ACCESS_FINE_LOCATION)
        if (granted) {
            val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            val isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            var latestLocation = getLatestLocation(LocationManager.GPS_PROVIDER, locationManager)
            if (!isGPSEnabled || !isNetworkEnabled)
                showSettingsAlert()
            if (latestLocation.isEmpty()) {
                latestLocation = getLatestLocation(LocationManager.NETWORK_PROVIDER, locationManager)
            } else {
                getPermission()
            }
        }
    }*/

    //Fungsi untuk mengambil latitude dan longitude dengan tepat.
    fun getLocation() {
        val granted = PermissionUtils.isGranted(this, PermissionEnum.ACCESS_FINE_LOCATION)
        if (granted) {
            val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            val isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            var latestLocation = getLatestLocation(LocationManager.GPS_PROVIDER, locationManager)
            //
            if (!isGPSEnabled || !isNetworkEnabled)
                showSettingsAlert()
            else
            {

                /* alert("Location is Detect", "Your location is detected. Lat : "+Utils.lat+ " Long : "+ Utils.long) {
                     yesButton {
                         it.dismiss()
                     }
                 }.show()*/
                latestLocation
                Toast.makeText(this, "Lat : "+Utils.lat+ " Long : "+ Utils.long, Toast.LENGTH_SHORT).show()
            }

            if (latestLocation.isEmpty()) {
                latestLocation = getLatestLocation(LocationManager.NETWORK_PROVIDER, locationManager)
            } else {
                getPermission()
            }
        }
    }

    //tanda ? disiini menandakan bahwa variabel
    fun resetIKR(){
        etFatForm1?.setText("")
        etFatForm2?.setText("")
        etFatForm3?.setText("")
        etFatForm4?.setText("")
        etId?.setText("")
    }

    fun resetSales(){
        formSales?.setText("")
    }

    fun ClearTake(){
        for (i in 0..2){
            Utils.arrayImage[i] = ""
            arrayImageData.set(0,ImageData("", "", "", "", ""))
        }

        RxBus.get().send(Utils.MENU)
        RxBus.get().send(Utils.RESET_TAKE)
    }

    fun clearDataSales(){
        Utils.FORM_SALES = ""

        for (i in 0..2){
            Utils.arrayImage[i] = ""
            arrayImageData.set(0,ImageData("", "", "", "", ""))
        }

        RxBus.get().send(Utils.MENU)
        RxBus.get().send(Utils.RESET_SALES)

    }

    fun clearDataIKR(){
        Utils.ACCOUNT_ID = ""
        Utils.FORM_IKR = ""
        Utils.FORM_IKR1 = ""
        Utils.FORM_IKR2 = ""
        Utils.FORM_IKR3 = ""
        Utils.FORM_IKR4 = ""


        for (i in 0..2){
            Utils.arrayImage[i] = ""
            //arrayImageData.set(0,ImageData("", "", "", "", ""))
        }

        RxBus.get().send(Utils.MENU)
        RxBus.get().send(Utils.RESET_IKR)

    }

    //fungsi untuk menanpilkan pemberitahuan tidak dapat mengambil lokasi
    fun showSettingsAlert(){
        alert("Can not get location", "Please enable GPS and Network") {
            yesButton {
                it.dismiss()
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
        }.show()
    }

    @SuppressLint("MissingPermission")
    fun getLatestLocation(provider:String, locationManager:LocationManager):Map<String, Any>{
        var result =  mutableMapOf<String, Any>()
        try {
            val location = locationManager.getLastKnownLocation(provider)
            if (location != null) {
                Utils.lat = location.latitude.toString()
                Utils.long = location.longitude.toString()
            }
        } catch (e: Throwable){
            toast( "lokasi gagal diterima $e")
        }
        return result
    }

    //Fungsi untuk mengecek Folder MKM_Pictures sudah ada atau belum, kalau belum maka akan terbentuk foldernya
    private fun checkDir(){
        val imgDir = File(Environment.getExternalStorageDirectory(), dir)
        if (!imgDir.exists()) {
            imgDir.mkdirs() // <----
            Toast.makeText(this, "Folder MKM_Pictures berhasil dibuat!", Toast.LENGTH_SHORT).show()
        }
        toast("{${Environment.getExternalStorageDirectory().toString() + dir}")
    }

    //Fungsi untuk menampilkan waktu ketika foto diambil
    private fun getDateTime(): String {
        val currentTime = DateFormat.getDateTimeInstance().format(Date())
        currentTime.toString()
        if (currentTime == "") {
            Toast.makeText(this, "Failed to get Date & Time. Please retake a Picture!", Toast.LENGTH_SHORT).show()
        }
        return currentTime
    }

    //fungsi untuk random number
    fun randNumber():Int{
        return Random().nextInt(10000) + 65
    }

    //Fungsi untuk membuat nama foto
    fun getImgName():String{
        return "MKM_"+randNumber()
    }

    private fun getCameraFragment(): CameraFragmentApi {
        return  getSupportFragmentManager().findFragmentByTag(Utils.CAMERA) as CameraFragmentApi
    }

    override fun onBackPressed() {
        when (supportFragmentManager.fragments.last()) {

            is LoginFragment -> {
                alert("Keluar dari Aplikasi?") {
                    yesButton {
                        finish()
                    }
                    noButton {
                        it.dismiss()
                    }
                }.show()
            }
            is MenuFragment -> {
                changeFragment(LoginFragment(), false, Utils.LOGIN)
            }

            is TakePicture -> {
                changeFragment(MenuFragment(), false, Utils.MENU)
                ClearTake()
            }
        }
    }
}