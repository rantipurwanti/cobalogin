package com.example.cobalogin.Services

import com.example.cobalogin.Model.FormIKR
import com.example.cobalogin.Model.FormSales
import com.example.cobalogin.Model.LoginRes
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST


interface MainService{
    @POST("upload")
    fun formSales(@Body data: FormSales): Observable<LoginRes>

    @POST("fat-upload")
    fun formIKR(@Body data: FormIKR): Observable<LoginRes>
}