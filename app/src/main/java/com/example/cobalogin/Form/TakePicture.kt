package com.example.cobalogin.Form

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cobalogin.Commons.RxBaseFragment
import com.example.cobalogin.Commons.RxBus
import com.example.cobalogin.Commons.Utils
import com.example.cobalogin.Model.ImageNum
import com.example.cobalogin.Model.ImageNumTake
import com.example.cobalogin.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_take_picture.*
import java.io.File


class TakePicture : RxBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_take_picture, container, false)
    }

    override fun onResume() {
        super.onResume()

        imgView4.setOnClickListener {
            RxBus.get().send(ImageNumTake(0))
        }
        imgView5.setOnClickListener {
            RxBus.get().send(ImageNumTake(1))
        }

        loadImage2()

    }

    private fun loadImage2(){
        if (!Utils.arrayImage[0].isNullOrBlank()){
            val file = File(Utils.arrayImage[0])
            Picasso.with(context).load(file).into(imgView4)
        }
        if(!Utils.arrayImage[1].isNullOrBlank()){
            val file = File (Utils.arrayImage[1])
            Picasso.with(context).load(file).into(imgView5)
        }
    }

    override fun onPause() {
        super.onPause()

    }
}
