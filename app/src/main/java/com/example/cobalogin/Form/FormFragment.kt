package com.example.cobalogin.Form

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cobalogin.Commons.RxBaseFragment
import com.example.cobalogin.Commons.RxBus
import com.example.cobalogin.Commons.Utils
import com.example.cobalogin.Model.DataIKR
import com.example.cobalogin.Model.DataSales
import com.example.cobalogin.Model.ImageNum
import com.example.cobalogin.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_form.*
import org.jetbrains.anko.support.v4.toast
import java.io.File

class FormFragment : RxBaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_form, container, false)
    }

    override fun onResume() {
        super.onResume()

        if (Utils.isSales)
            handleSales()
        else
            handleIKR()

        RxBus.get().send(Utils.HIDECAM)

        //Memanggil buttonImage imgView1 untuk mengambil gambar/foto
        imgView1.setOnClickListener {
            RxBus.get().send(ImageNum(0))
        }

        //Memanggil buttonImage imgView2 untuk mengambil gambar/foto
        imgView2.setOnClickListener {
            RxBus.get().send(ImageNum(1))
        }

        //Memanggil buttonImage imgView3 untuk mengambil gambar/foto
        imgView3.setOnClickListener {
            RxBus.get().send(ImageNum(2))
        }

        //Memanggil button btnCheckGPS untuk mengambil latitude dan longitude
        btnCheckGPS.setOnClickListener {
            RxBus.get().send(Utils.LOC)
        }

        //Memanggil button btnBack untuk kembali ke fragment sebelumnya
        btnBack.setOnClickListener {
            RxBus.get().send(Utils.MENU)
        }
        loadImage()
    }

    //Menampilkan gambar/foto yang telah tersimpan di array
    private fun loadImage() {
        if (!Utils.arrayImage[0].isNullOrBlank()){
            val file = File(Utils.arrayImage[0])
            Picasso.with(context).load(file).into(imgView1)
        }
        if (!Utils.arrayImage[1].isNullOrBlank()){
            val file = File(Utils.arrayImage[1])
            Picasso.with(context).load(file).into(imgView2)
        }
        if (!Utils.arrayImage[2].isNullOrBlank()){
            val file = File(Utils.arrayImage[2])
            Picasso.with(context).load(file).into(imgView3)
        }
    }

    //Mengcopy text yang ada pada EditText
    private fun handleIKR() {
        formSales.visibility = View.INVISIBLE
        formIKR.visibility = View.VISIBLE

        etFatForm1?.setText(Utils.FORM_IKR1)
        etFatForm2?.setText(Utils.FORM_IKR2)
        etFatForm3?.setText(Utils.FORM_IKR3)
        etFatForm4?.setText(Utils.FORM_IKR4)
        etId?.setText(Utils.ACCOUNT_ID)

        info1.text = "Dalam FAT"
        info2.text = "Luar FAT"
        info3.text = "Foto Rumah"

        etFatForm1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // TODO Auto-generated method stub
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                //menambahkan kondisi untuk mengecek panjang text, yang diberikan menurut persyaratan untuk ke editText berikutnya
                if (etFatForm1.getText().length == 5) {
                    etFatForm1.clearFocus()
                    etFatForm2.requestFocus()
                    etFatForm2.setCursorVisible(true)
                }
            }
        })

        etFatForm2.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // TODO Auto-generated method stub
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (etFatForm2.getText().length == 2) {
                    etFatForm2.clearFocus()
                    etFatForm3.requestFocus()
                    etFatForm3.setCursorVisible(true)
                }
            }
        })

        etFatForm3.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // TODO Auto-generated method stub
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (etFatForm3.getText().length == 2) {
                    etFatForm3.clearFocus()
                    etFatForm4.requestFocus()
                    etFatForm4.setCursorVisible(true)
                }
            }
        })

        etFatForm4.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // TODO Auto-generated method stub
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (etFatForm4.getText().length == 10) {
                    etFatForm4.clearFocus()
                }
            }
        })

        btnSend.setOnClickListener {
            if (!etFatForm1.text.isNullOrBlank() ||
                !etFatForm2.text.isNullOrBlank() ||
                !etFatForm3.text.isNullOrBlank() ||
                !etFatForm4.text.isNullOrBlank()){
                Log.d("dodol", "send click")
                val form = etFatForm1.text.toString()+"."+ etFatForm2.text.toString()+"."+ etFatForm3.text.toString()+"."+ etFatForm4.text.toString()
                Utils.ACCOUNT_ID = etId.text.toString()

                //mengirim data ke API DataIKR yang telah disediakan
                RxBus.get().send(DataIKR(
                    "${Utils.created_by}",
                    "$form",
                    "${Utils.ACCOUNT_ID}"
                ))
            } else
                toast("form harus diisi")
        }
    }

    private fun handleSales() {
        formSales.visibility = View.VISIBLE
        formIKR.visibility = View.INVISIBLE
        info1.text = "Rumah Pelanggan"
        info2.text = "App Form"
        info3.text = "KTP"

        formSales?.setText(Utils.FORM_SALES)

        //if >> Memeriksa data sales sudah terisi atau belum, kalau sudah data akan tersimpan
        btnSend.setOnClickListener {
            if (!formSales.text.isNullOrBlank()){

                Log.d("dodol", "send click")
                RxBus.get().send(DataSales(
                    "${Utils.created_by}",
                    "${formSales.text}"

                ))
            } else
                toast("form harus diisi")
        }
    }

    //menyimpan data ke API yang telah disediakan
    fun saveForm(){
        Utils.FORM_SALES = formSales?.text.toString()
        Utils.FORM_IKR = etFatForm1?.text.toString() + "." + etFatForm2?.text.toString() + "." + etFatForm3?.text.toString() + "." + etFatForm4?.text.toString()
        Utils.FORM_IKR1 = etFatForm1?.text.toString()
        Utils.FORM_IKR2 = etFatForm2?.text.toString()
        Utils.FORM_IKR3 = etFatForm3?.text.toString()
        Utils.FORM_IKR4 = etFatForm4?.text.toString()
        Utils.ACCOUNT_ID = etId?.text.toString()
    }

    override fun onPause() {
        super.onPause()
        saveForm()
    }

    override fun onDestroy() {
        super.onDestroy()
        //saveForm()
    }
}