package com.example.cobalogin.Model

data class DataIKR(
    val created_by: String,
    val fat_number: String,
    val account_id: String
)