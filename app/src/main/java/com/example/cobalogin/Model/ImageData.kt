package com.example.cobalogin.Model

data class ImageData (
    var img_file:String,
    var img_name:String,
    var lat:String,
    var lng:String,
    var datetime_taken:String
)