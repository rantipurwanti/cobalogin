package com.example.cobalogin.Model

data class FormSales(
    val data:MutableList<DataSales>,
    val images_attribute:MutableList<ImageData>
)