package com.example.cobalogin.Model

data class FormIKR(
    val data:MutableList<DataIKR>,
    val images_attribute:MutableList<ImageData>
)