package com.example.cobalogin.Login

import com.example.cobalogin.Model.Login
import com.example.cobalogin.Model.LoginRes
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService{
    @POST("sales-login")
    fun userLogin(@Body login: Login): Observable<LoginRes>
}