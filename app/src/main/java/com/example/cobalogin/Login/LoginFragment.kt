package com.example.cobalogin.Login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cobalogin.Commons.RxBaseFragment
import com.example.cobalogin.Commons.RxBus
import com.example.cobalogin.Commons.Utils
import com.example.cobalogin.Model.Login
import io.reactivex.android.schedulers.AndroidSchedulers
import com.example.cobalogin.R
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login.*
import okhttp3.OkHttpClient
import org.jetbrains.anko.support.v4.toast
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class LoginFragment : RxBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onResume() {
        super.onResume()
        //RxBus.get().send(Utils.LOC)
        btnLogin.setOnClickListener {
            if (!etSalescode.text.toString().isNullOrEmpty()) {
                Utils.created_by = etSalescode.text.toString()
                validateUser(etSalescode.text.toString())
            } else
                toast("isi sales code")
        }
    }

    private fun validateUser(salesCode:String) {
        val login = Login(salesCode)
        subscriptions.add(provideLoginService().userLogin(login)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                    user ->
                if (user.code != "login_failed") {
                    RxBus.get().send(Utils.MENU)
                } else {
                    toast("${user.message}")
                }
            }, {
                    _ -> toast("connection error")
            }))
    }

    fun provideLoginService(): LoginService {

        val clientBuilder: OkHttpClient.Builder = Utils.buildClient()

        val retrofit = Retrofit.Builder()
            .baseUrl(Utils.DEV_ENDPOINT)
            .client(clientBuilder.build())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
        return retrofit.create(LoginService::class.java)
    }


}
